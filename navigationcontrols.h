#ifndef NAVIGATIONCONTROLS_H
#define NAVIGATIONCONTROLS_H

#include "controls.h"
#include "mnt.h"

class NavigationControls : public Controls
{
public:
    NavigationControls(GLFWwindow* window, Camera *camera);
    void update(float deltaTime, Shader *shader);
    void setMNT(MNT *_mnt) {mnt = _mnt;}
    MNT * mnt;
private:
    glm::vec2 lastPosCursor;
};

#endif // NAVIGATIONCONTROLS_H
