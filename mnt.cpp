#include "mnt.h"
#include <iostream>
#include <bits/stdc++.h>
#include <vector>

using std::vector;
using namespace std;

MNT::MNT(int _rows,int _res, int hmax)
{
    rows = _rows;
    res = _res;

    for (int i=0; i<rows; i++) {
        for (int j=0; j<rows; j++) {
            arr[i][j] = 1 + (rand() % hmax) + 10/(sqrt((rows/2-i)*(rows/2-i)+(rows/2-j)*(rows/2-j))+1);
        }
    }

    boundBox = glm::vec4(res,res*(rows-2),res,res*(rows-2));
}


Object MNT::getMNTObject() {

    std::vector<glm::vec3> vertex_buffer_data;
    std::vector<glm::vec2> uv_buffer_data;

    for (int i=1; i<rows-1; i++)
    {
        for (int j=1; j<rows-1; j++)
        {
            // first triangle
            vertex_buffer_data.push_back(glm::vec3(i*res,arr[i][j],j*res));
            uv_buffer_data.push_back(glm::vec2(0.000059f, 1.0f-0.000004f));
            vertex_buffer_data.push_back(glm::vec3((i+1)*res,arr[i+1][j],j*res));
            uv_buffer_data.push_back(glm::vec2(0.000103f, 1.0f-0.336048f));
            vertex_buffer_data.push_back(glm::vec3(i*res,arr[i][j+1],(j+1)*res));
            uv_buffer_data.push_back(glm::vec2(0.335973f, 1.0f-0.335903f));

            // second triangle
            vertex_buffer_data.push_back(glm::vec3(i*res,arr[i][j],j*res));
            uv_buffer_data.push_back(glm::vec2(0.000059f, 1.0f-0.000004f));
            vertex_buffer_data.push_back(glm::vec3((i-1)*res,arr[i-1][j],j*res));
            uv_buffer_data.push_back(glm::vec2(0.000103f, 1.0f-0.336048f));
            vertex_buffer_data.push_back(glm::vec3(i*res,arr[i][j-1],(j-1)*res));
            uv_buffer_data.push_back(glm::vec2(0.335973f, 1.0f-0.335903f));
        }
    }

    Object objectMNT(vertex_buffer_data, uv_buffer_data, "../mntopengl/textures/eau.jpg");
    return objectMNT;
}

float MNT::getHeight(float x, float y) {
    int i = floor(x/res);
    int j = floor(y/res);

    float max1 = std::max(arr[i][j],arr[i+1][j]);
    float max2 = std::max(arr[i+1][j+1],arr[i][j+1]);
    return std::max(max1,max2);
}
