#ifndef MNT_H
#define MNT_H

#include <vector>
#include "glm/glm.hpp"
#include "object.h"

class MNT
{
private:
    int rows;
    int res;
    float arr[100][100];

public:
    MNT(int rows,int res, int hmax);

    int getRows() {return rows;}
    int getRes() {return res;}
    Object getMNTObject();

    glm::vec4 boundBox;
    float getHeight(float x, float y);
};

#endif // MNT_H
