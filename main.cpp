#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "glm/glm.hpp"
#include <vector>
#include <cmath>

#include "vertexbuffer.h"
#include "vertexarray.h"
#include "shader.h"
#include "renderer.h"
#include "camera.h"
#include "navigationcontrols.h"
#include "mnt.h"
#include "cube.h"


using namespace std;

int main()
{

/////////////////////////Initialisation de GLFW/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if(!glfwInit()){
        fprintf(stderr, "Failed to initialize GLFW\n");
        return -1;
    }


    glfwWindowHint(GLFW_SAMPLES, 4); //antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //version 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //version core

    // Cull triangles which normal is not towards the camera
    glEnable(GL_CULL_FACE);


/////////////////////////Ouverture de la fenêtre/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //définition de la taille de la fenêtre
    int width=1000;
    int height=1000;

    //On récupère les dimensions de l'écran pour créer la fenètre
    //GLFWmonitor* primary = glfwGetPrimaryMonitor();
    //glfwGetMonitorWorkarea(primary,nullptr,nullptr, &width, &height);

    //Enfin on crée la fenêtre
    GLFWwindow* window = glfwCreateWindow(width,height,"OpenGL_API",NULL,NULL);
    glfwSwapInterval(1);
    //On vérifie que l'initialisation a bien marché
    if (window==NULL){
        fprintf(stderr, "Erreur lors de la création de la fénêtre\n");
        glfwTerminate();
        return -1;
    }

    //Enfin on définit la fenêtre créée comme la fenêtre sur laquelle on va dessiner
    glfwMakeContextCurrent(window);



/////////////////////////Initialisation de GLEW/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Initialisation de GLEW
    glewExperimental=true;
    if (glewInit() != GLEW_OK){
        fprintf(stderr, "Erreur lors de l'initialisation de GLEW\n");
        return -1;
    }

/////////////////////////On crée un Renderer/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    Renderer renderer;


/////////////////////////On crée un Shader/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    Shader shader("../mntopengl/shaders/SimpleVertexShader.vertexshader", "../mntopengl/shaders/SimpleFragmentShader.fragmentshader");
    shader.Bind();

/////////////////////////On crée un vertex array/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    VertexArray va;
    va.Bind();

/////////////////////////Création des objets/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    MNT mnt(30,3,3);
    Object objectMNT = mnt.getMNTObject();
    Cube cube;
    Object objectCube(cube.getVertexBufferData(),cube.getUVBufferData(),"../mntopengl/textures/dauphin.png");
    float posxCubeInit = mnt.getRows()*mnt.getRes()/2;
    float posyCubeInit = mnt.getRows()*mnt.getRes()/2;
    float poszCubeInit = mnt.getHeight(posxCubeInit,posyCubeInit) + 10;
    objectCube.position = glm::vec3(posxCubeInit,poszCubeInit,posyCubeInit);


/////////////////////////Création de la camera et les contrôles/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    Camera cam(width, height);
    NavigationControls controls(window, &cam);
    float posxCamInit = mnt.getRows()*mnt.getRes()/2;
    float posyCamInit = mnt.getRows()*mnt.getRes()/2;
    float poszCamInit = mnt.getHeight(posxCamInit,posyCamInit) + 10;
    cam.position = glm::vec3(posxCamInit,poszCamInit,posyCamInit);
    cam.horizontalAngle = 0.f*M_PI/180;
    cam.verticalAngle = 0.f*M_PI/180;

    controls.setMNT(&mnt);

/////////////////////////Initialisation des matrices MVP/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    cam.computeMatrices(width, height);
    glm::mat4 v = cam.getViewMatrix();
    glm::mat4 p = cam.getProjectionMatrix();

    glm::mat4 mMNT = objectMNT.getModelMatrix();
    glm::mat4 mvpMNT = p*v*mMNT;

    glm::mat4 mCube = objectCube.getModelMatrix();
    glm::mat4 mvpCube = p*v*mCube;

    shader.setUniformMat4f("MVP", mvpMNT);


/////////////////////////Boucle de rendu/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    // Assure que l'on peut capturer les touche de clavier
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    //On indique la couleur de fond
    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

    //On autorise les tests de profondeur

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    float lastTime = glfwGetTime();
    float currentTime, deltaTime;

    while(glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(window)){

        currentTime = glfwGetTime();
        deltaTime = currentTime-lastTime;
        lastTime = currentTime;

        // cube position update
        float PosxCubeNew = posxCubeInit+std::cos(currentTime)*10;
        float PosyCubeNew = posyCubeInit;
        float PoszCubeCurrent = objectCube.position[1];
        float PoszCubeNew = PoszCubeCurrent + (mnt.getHeight(PosxCubeNew,PosyCubeNew) - PoszCubeCurrent) * 0.2 + 1;
        objectCube.position = glm::vec3(PosxCubeNew,PoszCubeNew,PosyCubeNew);

        controls.update(deltaTime, &shader);

        // computing mvp matrices
        cam.computeMatrices(width, height);
        v = cam.getViewMatrix();
        p = cam.getProjectionMatrix();

        mMNT = objectMNT.getModelMatrix();
        mvpMNT = p*v*mMNT;

        mCube = objectCube.getModelMatrix();
        mvpCube = p*v*mCube;

        // vider les buffers
        renderer.Clear();

        // dessiner les objets
        shader.setUniformMat4f("MVP", mvpMNT);
        renderer.Draw(va, objectMNT, shader);
        shader.setUniformMat4f("MVP", mvpCube);
        renderer.Draw(va, objectCube, shader);

        ////////////////Partie rafraichissement de l'image et des évènements///////////////
        //Swap buffers : frame refresh
        glfwSwapBuffers(window);
        //get the events
        glfwPollEvents();
    }
    glfwTerminate();

    return 0;

}
