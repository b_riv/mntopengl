#include "navigationcontrols.h"
#include "../mntopengl/imgui/imgui.h"

NavigationControls::NavigationControls(GLFWwindow *window, Camera *camera):Controls(window, camera), lastPosCursor(-1,-1)
{
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    mouseSpeed = 0.02;
}

void NavigationControls::update(float deltaTime, Shader *shader)
{
    glm::vec4 bbx = mnt->boundBox;
    // On test d'abord si on est sur la fenêtre imgui ou pas
    if (/*!io.WantCaptureMouse*/true){

        int state = glfwGetMouseButton(m_Window, GLFW_MOUSE_BUTTON_LEFT);
        double xpos, ypos;
        glfwGetCursorPos(m_Window, &xpos, &ypos);
        if(state == GLFW_PRESS){
            if (lastPosCursor.x!=-1){
                m_Camera->horizontalAngle+= mouseSpeed * deltaTime * float( xpos-lastPosCursor.x );
                m_Camera->verticalAngle  += mouseSpeed * deltaTime * float( ypos-lastPosCursor.y );
            }
            lastPosCursor.x = xpos;
            lastPosCursor.y = ypos;
        }
        else{
           lastPosCursor = glm::vec2(-1,-1);
        }


        glm::vec3 direction(
            cos(m_Camera->verticalAngle) * sin(m_Camera->horizontalAngle),
            sin(m_Camera->verticalAngle),
            cos(m_Camera->verticalAngle) * cos(m_Camera->horizontalAngle)
        );

        // Right vector
        glm::vec3 right = glm::vec3(
            sin(m_Camera->horizontalAngle - 3.14f/2.0f),
            0,
            cos(m_Camera->horizontalAngle - 3.14f/2.0f)
        );

        // Up vector : perpendicular to both direction and right
        glm::vec3 up = glm::cross( right, direction );

        // Move forward
        if (glfwGetKey(m_Window, GLFW_KEY_UP ) == GLFW_PRESS){
            glm::vec3 new_position = m_Camera->position + direction * deltaTime * speed;
            float minMNT = mnt->getHeight(new_position[0],new_position[2]) + 1;
            if ((new_position[0] > bbx[0]) && (new_position[0] < bbx[1]) && (new_position[2] > bbx[2]) && (new_position[2] < bbx[3]) && (new_position[1] > minMNT))
            {
                m_Camera->position = new_position;
            }
        }
        // Move backward
        if (glfwGetKey(m_Window, GLFW_KEY_DOWN ) == GLFW_PRESS){
            glm::vec3 new_position = m_Camera->position - direction * deltaTime * speed;
            float minMNT = mnt->getHeight(new_position[0],new_position[2]) + 1;
            if ((new_position[0] > bbx[0]) && (new_position[0] < bbx[1]) && (new_position[2] > bbx[2]) && (new_position[2] < bbx[3]) && (new_position[1] > minMNT))
            {
                m_Camera->position = new_position;
            }
        }
        // Strafe right
        if (glfwGetKey(m_Window, GLFW_KEY_RIGHT ) == GLFW_PRESS){
            glm::vec3 new_position = m_Camera->position + right * deltaTime * speed;
            float minMNT = mnt->getHeight(new_position[0],new_position[2]) + 1;
            if ((new_position[0] > bbx[0]) && (new_position[0] < bbx[1]) && (new_position[2] > bbx[2]) && (new_position[2] < bbx[3]) && (new_position[1] > minMNT))
            {
                m_Camera->position = new_position;
            }
        }
        // Strafe left
        if (glfwGetKey(m_Window, GLFW_KEY_LEFT ) == GLFW_PRESS){
            glm::vec3 new_position = m_Camera->position - right * deltaTime * speed;
            float minMNT = mnt->getHeight(new_position[0],new_position[2]) + 1;
            if ((new_position[0] > bbx[0]) && (new_position[0] < bbx[1]) && (new_position[2] > bbx[2]) && (new_position[2] < bbx[3]) && (new_position[1] > minMNT))
            {
                m_Camera->position = new_position;
            }
        }
        // Move forward
        if (glfwGetKey(m_Window, GLFW_KEY_W ) == GLFW_PRESS){
            glm::vec3 new_position = m_Camera->position + direction * deltaTime * speed;
            float minMNT = mnt->getHeight(new_position[0],new_position[2]) + 1;
            if ((new_position[0] > bbx[0]) && (new_position[0] < bbx[1]) && (new_position[2] > bbx[2]) && (new_position[2] < bbx[3]) && (new_position[1] > minMNT))
            {
                m_Camera->position = new_position;
            }
        }
        // Move backward
        if (glfwGetKey(m_Window, GLFW_KEY_S ) == GLFW_PRESS){
            glm::vec3 new_position = m_Camera->position - direction * deltaTime * speed;
            float minMNT = mnt->getHeight(new_position[0],new_position[2]) + 1;
            if ((new_position[0] > bbx[0]) && (new_position[0] < bbx[1]) && (new_position[2] > bbx[2]) && (new_position[2] < bbx[3]) && (new_position[1] > minMNT))
            {
                m_Camera->position = new_position;
            }
        }
        // Strafe right
        if (glfwGetKey(m_Window, GLFW_KEY_D ) == GLFW_PRESS){
            glm::vec3 new_position = m_Camera->position + right * deltaTime * speed;
            float minMNT = mnt->getHeight(new_position[0],new_position[2]) + 1;
            if ((new_position[0] > bbx[0]) && (new_position[0] < bbx[1]) && (new_position[2] > bbx[2]) && (new_position[2] < bbx[3]) && (new_position[1] > minMNT))
            {
                m_Camera->position = new_position;
            }
        }
        // Strafe left
        if (glfwGetKey(m_Window, GLFW_KEY_A ) == GLFW_PRESS){
            glm::vec3 new_position = m_Camera->position - right * deltaTime * speed;
            float minMNT = mnt->getHeight(new_position[0],new_position[2]) + 1;
            if ((new_position[0] > bbx[0]) && (new_position[0] < bbx[1]) && (new_position[2] > bbx[2]) && (new_position[2] < bbx[3]) && (new_position[1] > minMNT))
            {
                m_Camera->position = new_position;
            }
        }
        // go up
        if (glfwGetKey(m_Window, GLFW_KEY_SPACE ) == GLFW_PRESS){
            glm::vec3 new_position = m_Camera->position + up * deltaTime * speed;
            if ((new_position[0] > bbx[0]) && (new_position[0] < bbx[1]) && (new_position[2] > bbx[2]) && (new_position[2] < bbx[3]))
            {
                m_Camera->position = new_position;
            }
        }
        // go down
        if (glfwGetKey(m_Window, GLFW_KEY_LEFT_SHIFT ) == GLFW_PRESS){
            glm::vec3 new_position = m_Camera->position - up * deltaTime * speed;
            float minMNT = mnt->getHeight(new_position[0],new_position[2]) + 1;
            std::cout << minMNT;
            if ((new_position[0] > bbx[0]) && (new_position[0] < bbx[1]) && (new_position[2] > bbx[2]) && (new_position[2] < bbx[3]) && (new_position[1] > minMNT))
            {
                m_Camera->position = new_position;
            }
        }
    }
}



