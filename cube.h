#ifndef CUBE_H
#define CUBE_H

#include <vector>
#include "glm/glm.hpp"

class Cube
{
public:
    Cube();
    std::vector<glm::vec2> getUVBufferData();
    std::vector<glm::vec3> getVertexBufferData();
};

#endif // CUBE_H
