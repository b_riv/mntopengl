# TP open GL

Projet réalisé à partir du TP openGL en TSI à l'ENSG Géomatique

La scène montre un MNT généré aléatoirement, celui-ci est texturé.

Un objet se déplace au dessus de ce MNT en prenant en compte la hauteur du terrain

L'utilisateur peut se déplacer dans les limites du MNT et ne peut pas le traverser


## lancement de l'application

Lancer l'application sur QTcreator, un fichier build sera créé dans le dossier parent de la racine de ce document.

Dans ce cas les chemins relatifs devraient être valides
