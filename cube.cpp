#include "cube.h"

Cube::Cube()
{

}


std::vector<glm::vec3> Cube::getVertexBufferData() {
    std::vector<glm::vec3> g_vertex_buffer_data = {
        glm::vec3(-1.0f,-1.0f,-1.0f), // triangle 1 : begin
        glm::vec3(-1.0f,-1.0f, 1.0f),
        glm::vec3(-1.0f, 1.0f, 1.0f), // triangle 1 : end
        glm::vec3(1.0f, 1.0f,-1.0f), // triangle 2 : begin
        glm::vec3(-1.0f,-1.0f,-1.0f),
        glm::vec3(-1.0f, 1.0f,-1.0f), // triangle 2 : end
        glm::vec3(1.0f,-1.0f, 1.0f),
        glm::vec3(-1.0f,-1.0f,-1.0f),
        glm::vec3(1.0f,-1.0f,-1.0f),
        glm::vec3(1.0f, 1.0f,-1.0f),
        glm::vec3(1.0f,-1.0f,-1.0f),
        glm::vec3(-1.0f,-1.0f,-1.0f),
        glm::vec3(-1.0f,-1.0f,-1.0f),
        glm::vec3(-1.0f, 1.0f, 1.0f),
        glm::vec3(-1.0f, 1.0f,-1.0f),
        glm::vec3(1.0f,-1.0f, 1.0f),
        glm::vec3(-1.0f,-1.0f, 1.0f),
        glm::vec3(-1.0f,-1.0f,-1.0f),
        glm::vec3(-1.0f, 1.0f, 1.0f),
        glm::vec3(-1.0f,-1.0f, 1.0f),
        glm::vec3(1.0f,-1.0f, 1.0f),
        glm::vec3(1.0f, 1.0f, 1.0f),
        glm::vec3(1.0f,-1.0f,-1.0f),
        glm::vec3(1.0f, 1.0f,-1.0f),
        glm::vec3(1.0f,-1.0f,-1.0f),
        glm::vec3(1.0f, 1.0f, 1.0f),
        glm::vec3(1.0f,-1.0f, 1.0f),
        glm::vec3(1.0f, 1.0f, 1.0f),
        glm::vec3(1.0f, 1.0f,-1.0f),
        glm::vec3(-1.0f, 1.0f,-1.0f),
        glm::vec3(1.0f, 1.0f, 1.0f),
        glm::vec3(-1.0f, 1.0f,-1.0f),
        glm::vec3(-1.0f, 1.0f, 1.0f),
        glm::vec3(1.0f, 1.0f, 1.0f),
        glm::vec3(-1.0f, 1.0f, 1.0f),
        glm::vec3(1.0f,-1.0f, 1.0f)
    };
    return g_vertex_buffer_data;
}

std::vector<glm::vec2> Cube::getUVBufferData() {
    std::vector<glm::vec2> g_uv_buffer_data = {
        glm::vec2(0,0),
        glm::vec2(0,1),
        glm::vec2(1,0),
        glm::vec2(1.0f,1.0f),
        glm::vec2(1.0f, 0.0f),
        glm::vec2(0.0f,1.0f),
        glm::vec2(0.0f,1.f),
        glm::vec2(1.0f,1.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(1.0f,1.0f),
        glm::vec2(1.0f,0.0f),
        glm::vec2(0.0f,1.f),
        glm::vec2(1.0f,1.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(1.0f,1.0f),
        glm::vec2(1.0f,0.0f),
        glm::vec2(0.0f,1.f),
        glm::vec2(1.0f,1.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(1.0f,1.0f),
        glm::vec2(1.0f,0.0f),
        glm::vec2(0.0f,1.f),
        glm::vec2(1.0f,1.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(1.0f,1.0f),
        glm::vec2(1.0f,0.0f),
        glm::vec2(0.0f,1.f),
        glm::vec2(1.0f,1.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(0.0f, 0.0f),
        glm::vec2(1.0f,1.0f),
        glm::vec2(1.0f,0.0f)
    };
    return g_uv_buffer_data;
}
